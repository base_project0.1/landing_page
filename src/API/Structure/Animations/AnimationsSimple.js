import TweenMax from 'gsap'

export default {

  Pulsing,
  Blinking,

  Movement,
  Entrance,

  BackgroundColor,
  Color,

  Show,
  Hide,

  Appear,
  Desappear,

  GrowUp,

  To,
  From,
  FromTo,

}

function Pulsing (element, scale, time) {
  scale = scale || 1.1
  return TweenMax.to(element,  time, {scaleX:scale, scaleY:scale, repeat:-1, yoyo:true});
}

function Blinking (element, time) {
  return TweenMax.to(element,  time, {autoAlpha:0, repeat:-1, yoyo:true});
}

function Movement (element, movement, time) {
  movement = movement || {x: 500}
  movement['ease'] = Power0.easeOut;
  return TweenMax.to(element,  time, movement);
}

function Entrance (element, movement, time) {
  movement = movement || {x: 500}
  movement['ease'] = Power0.easeOut;
  return TweenMax.from(element,  time, movement);
}

function BackgroundColor (element, color, time) {
  return TweenMax.to(element,  time, {backgroundColor: color || "black"})
}

function Color (element, color, time) {
  return TweenMax.to(element,  time, {color: color, ease:Back.easeOut})
}

function Show (element, time) {
  return TweenMax.to(element,  time, {autoAlpha:1});
}

function Hide(element, time) {
  return TweenMax.to(element,  time, {autoAlpha:0});
}

function Appear (element, time) {
  return TweenMax.fromTo(element,  time, {autoAlpha:0}, {autoAlpha:1});
}


function Desappear (element, params, time ) {console.log();
  return TweenMax.fromTo(element,  time, {autoAlpha:1}, {autoAlpha:0, onComplete: params.onComplete});
}

function GrowUp (element, scale, time) {
  scale = scale || 1.1
  return TweenMax.to(element,  time, {scaleX:scale, scaleY:scale});
}

function To (element, params, time) {
  return TweenMax.to(element,  time, params);
}

function From (element, params, time) {
  return TweenMax.from(element,  time, params);
}

function FromTo (element, params, time) {
    return TweenMax.fromTo(element,  time, params.from, params.to);
}
