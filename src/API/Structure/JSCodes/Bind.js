export default function (self, functionsList) {
  functionsList.map((bind) => {
    self[bind] = self[bind].bind(self)
  })
}
