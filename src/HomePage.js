//React
import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Api } from 'lib'

//Reducers
// import { actions as ExempleActions } from 'reducers/ExempleActions'

//Responsible
import { Grid, Row, Col } from 'react-flexbox-grid'
//http://www.redbitdev.com/getting-started-with-react-flexbox-grid/

//Styles
import Style from './style'

//Components
import Header from './components/Header/index.js'

//Others
import Bind from 'API_Structure/JSCodes/Bind'

class HomePage extends React.Component {

  constructor(props) {
    super(props)
  }

  render() {

    return (
      <Style>
        <Header />
      </Style>
    )
    
  }
}

function mapStateToProps({ /* name at combineReducers, of reducers/index "Example" */ }) {

  return {
    //return de reducers parameters on state
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      // name of all import reducers
      // ...ExampleActions,
    }, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage)
