//React
import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Api } from 'lib'

//Reducers
// import { actions as ExempleActions } from 'reducers/ExempleActions'

//Responsible
import { Grid, Row, Col } from 'react-flexbox-grid'
//http://www.redbitdev.com/getting-started-with-react-flexbox-grid/

//Styles
import Style from './style'

//Others
import Bind from 'API_Structure/JSCodes/Bind'

class HeaderMenu extends React.Component {

  constructor(props) {
    super(props)
  }



  render() {

    const { menuOpened, toggleMenu } = this.props

    console.log(menuOpened, toggleMenu)

    return (
        <div 
            className={`header__menu-icon js-menu-icon ${menuOpened && 'menu-icon-opened'}`}
            onClick={ () => toggleMenu() }
        >
            <div className="header__menu-icon__line"></div>
            <div className="header__menu-icon__line"></div>
            <div className="header__menu-icon__line"></div>
        </div>
    )
    
  }
}

function mapStateToProps({ /* name at combineReducers, of reducers/index "Example" */ }) {

  return {
    //return de reducers parameters on state
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      // name of all import reducers
      // ...ExampleActions,
    }, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMenu)
