//React
import React, { Component } from 'react'
import PropTypes from 'prop-types'

//API & Reducers
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Api } from 'lib'

//Reducers
// import { actions as ExempleActions } from 'reducers/ExempleActions'

//Responsible
import { Grid, Row, Col } from 'react-flexbox-grid'
//http://www.redbitdev.com/getting-started-with-react-flexbox-grid/

//Styles
import Style from './style'

//Components
import HeaderBtn from './header-btn'

//Others
import Bind from 'API_Structure/JSCodes/Bind'

class HeaderMenu extends React.Component {

  constructor(props) {
    super(props)
    
    this.state = {
        menuOpened: false
    }
  }

  toggleMenu = () => {
      this.setState({ menuOpened: !this.state.menuOpened })
  }

  render() {

    const { menuOpened } = this.state

    return (
      <Style>
        <div className="header">
            
            <HeaderBtn menuOpened={menuOpened} toggleMenu={this.toggleMenu} />
            
            <div className={`header__menu ${menuOpened && 'overlay-opened'}`}>
                <ul className="main-menu">
                    <li className="menu__item"><a href="#">Institucional</a></li>
                    <li className="menu__item"><a href="#">Contato</a></li>
                </ul>
            </div>
        </div>
      </Style>
    )
    
  }
}

function mapStateToProps({ /* name at combineReducers, of reducers/index "Example" */ }) {

  return {
    //return de reducers parameters on state
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({
      // name of all import reducers
      // ...ExampleActions,
    }, dispatch),
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(HeaderMenu)
