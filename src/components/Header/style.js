import styled from 'styled-components'

const style = styled.div`

    .header {

        .header__menu-icon {
            width: 30px;
            height: 30px;
            position: absolute;
            right: 15px;
            display: flex;
            flex-direction: column;
            justify-content: space-between;
            cursor: pointer;
            z-index: 3;

            .header__menu-icon__line {
                width: 30px;
                height: 5px;
                background-color: #fff;
                border-radius: 1px;
                transform-origin: top;

                transition: .3s ease;
            }
        }

        .header__menu-icon.menu-icon-opened {

            .header__menu-icon__line {
                &:nth-child(1) {
                    transform: rotate(45deg) translateX(11px) translateY(10px);
                }
                &:nth-child(2) {
                    opacity: 0;
                }
                &:nth-child(3) {
                    transform: rotate(-45deg) translateX(5px) translateY(-10px);
                }
            }

        }

        .header__menu {
            width: 100%;
            min-height: 100vh;
            background-color: #37d16b;
            display: flex;
            justify-content: center;
            align-items: center;
            position: fixed;
            left: 0;
            top: 0;
            opacity: 0;

            transition: .3s ease;

            ul.main-menu {
                margin: 0;
                padding: 0;
                text-align: center;
                list-style-type: none;

                li.menu__item {
                    display: block;

                    a {
                        font-family: GTPressura-mono;
                        text-transform: uppercase;
                        color: #fff;
                        font-size: 60px;
                        line-height: 2;
                        font-weight: 300;

                        cursor: pointer;
                        text-decoration: none;

                        transition: .3s ease;

                        :hover {
                            font-weight: 600;
                        }
                    }
                }
            }
        }

        .header__menu.overlay-opened {
            opacity: 1;
            visibility: visible;
            z-index: 2;
        }

    }

`
export default style
