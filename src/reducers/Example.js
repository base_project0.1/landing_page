import { Api } from 'lib'

const initialState = {
  //set all init variables
}

const types = {
  //set types of dispatch, try keep the same name... Example:
  // FETCH_SUCCESS: 'FETCH_SUCCESS',
}
export const actions = {
  //set all actions of the reducers, that be acess by another scripts... Example
  // setAnything: () => (dispatch) => {
  //   return  dispatch({
  //             type: types.FETCH_SUCCESS,
  //             payload: "any_Data",
  //           }))
  // },
}
export default (state = initialState, { type, payload }) => {
  //according with the type, return a new state
  switch (type) {
    // Example:
    case types.FETCH_SUCCESS:
      return {
        ...state,
        anyVar: payload,
      }
    default:
      return state
  }
}
